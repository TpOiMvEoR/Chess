﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace DB_with_WEB_SERVICES
{
    public class Connection
    {
        private SqlConnection conn;
        private SqlDataAdapter Ad;
        private SqlCommand cmd;
        public Connection()
        {
            this.conn = new SqlConnection(@"Data Source='TPMAGSHIMIM\SQLEXPRESS01'; Initial Catalog=Chess ; Integrated Security=SSPI");
        }
        public Connection(string tablename)
        {
            this.conn = new SqlConnection(@"Data Source='TPMAGSHIMIM\SQLEXPRESS01'; Initial Catalog=" + tablename + "; Integrated Security=SSPI");
        }
        public SqlDataReader getDataReader(string sqlstr)
        {
            this.cmd = new SqlCommand(sqlstr, this.conn);
            SqlDataReader dr = cmd.ExecuteReader();
            return dr;
        }
        public SqlDataReader getDataReader2(string sqlStr)
        {
            conn.Open();
            SqlCommand command = new SqlCommand(sqlStr, this.conn);
            return command.ExecuteReader();
        }
        public DataSet getDataSet(string sqlstr, string tablename)
        {
            this.cmd = new SqlCommand(sqlstr, this.conn);
            this.Ad = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            Ad.Fill(ds, tablename);
            return ds;
        }
        public DataSet getDataSet2(string sqlstr, string tablename)
        {
            this.cmd = new SqlCommand(sqlstr, this.conn);
            this.Ad = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            Ad.Fill(ds, tablename);
            return ds;
        }
        public DataSet getDataSet(string tablename)
        {
            string sql = "SELECT * FROM [" + tablename + "]";
            SqlCommand command = new SqlCommand(sql, this.conn);
            this.Ad = new SqlDataAdapter(command);
            DataSet dataset = new DataSet();
            this.Ad.Fill(dataset, tablename);
            return dataset;
        }
        public void UpdateDataSet(DataSet dataset, string tablename)
        {
            this.Ad.Fill(dataset, tablename);
            SqlCommandBuilder builder = new SqlCommandBuilder(Ad);
            Ad.UpdateCommand = builder.GetUpdateCommand();
            Ad.InsertCommand = builder.GetInsertCommand();
            Ad.DeleteCommand = builder.GetDeleteCommand();
            Ad.Update(dataset, tablename);
        }

        public int Insert(string sqlstr)
        {
            ConnectionOpen();
            this.cmd = new SqlCommand(sqlstr, this.conn);
            int num = this.cmd.ExecuteNonQuery();
            ConnectionClose();
            return num;
        }
        public void ConnectionOpen()
        {
            this.conn.Open();
        }
        public void ConnectionClose()
        {
            this.conn.Close();
        }
        public bool ExecuteConnectionSql(string sql)
        {
            cmd = new SqlCommand(sql, this.conn);
            int num = cmd.ExecuteNonQuery();
            return num > 0;
        }

        public void UpdateDataSet(DataSet ds)
        {
            SqlCommandBuilder builder = new SqlCommandBuilder(Ad);
            //Ad.UpdateCommand = builder.GetUpdateCommand();
            Ad.InsertCommand = builder.GetInsertCommand();
            //Ad.DeleteCommand = builder.GetDeleteCommand();
            Ad.Update(ds.Tables[0]);
        }
        public void UpdateDataSet2(DataSet ds)
        {
            SqlCommandBuilder builder = new SqlCommandBuilder(Ad);
            Ad.UpdateCommand = builder.GetUpdateCommand();
            Ad.InsertCommand = builder.GetInsertCommand();
            Ad.DeleteCommand = builder.GetDeleteCommand();
            Ad.Update(ds.Tables[0]);
        }
    }
}