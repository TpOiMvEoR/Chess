﻿using System;
using System.Drawing;

namespace Server
{
    //special movement, therefore combined is way clear in valid move
    public class Pawn : Figure
    {
        /// <summary>
        /// constractor
        /// </summary>
        /// <param name="place">point that represents the place of the figure</param>
        /// <param name="data">string that contains all the board information</param>
        public Pawn(Point place, string data)
            : base(place, data)
        {
        }

        /// <summary>
        /// constractor
        /// </summary>
        /// <param name="place">point that represents the place of the figure</param>
        /// <param name="board">class that contains all the board information</param>
        public Pawn(Point place, Board board)
            : base(place, board)
        {
        }

        /// <summary>
        /// override to match for current figure movement
        /// </summary>
        /// <param name="dst">the place the figure want to end up at</param>
        /// <returns>if the move logicly valid</returns>
        public override bool ValidMove(Point dst)   //clear map move validity
        {
            char c = _board.GetCharInDest(_place);
            bool black = c < 'a';

            if (_place.X == dst.X)
            {
                if (_board.GetCharInDest(dst) != '#')   //pawn can attack only up with side
                    return false;

                if (_place.Y - dst.Y == (black ? -1 : 1))
                    return true;

                if (_place.Y - dst.Y == (black ? -2 : 2) && _place.Y == (black ? 1 : 6) && _board.GetCharInDest(new Point(_place.X, _place.Y + (black ? 1 : -1))) == '#')
                    return true;
            }
            else
            {
                if (Math.Abs(_place.X - dst.X) == 1 && _place.Y - dst.Y == (black ? -1 : 1) && _board.GetCharInDest(dst) != '#')
                    return true;
            }

            return false;
        }
    }
}