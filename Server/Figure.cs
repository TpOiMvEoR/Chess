﻿using System.Drawing;

namespace Server
{
    //base class for all figures
    public class Figure
    {
        protected Point _place;  //the placement of the current figure
        protected char _type;     //the type of the current figure
        protected Board _board;

        private const int OK = 0;
        //protected const int SAME_PLACE = 1;
        //protected const int ALLY_FIGURE_IN_DST = 2;
        private const int FIGURES_IN_THE_WAY = 1;
        private const int INVALID_MOVE_FOR_FIGURE = 2;

        //represents the board
        public class Board
        {
            public char[,] _board { get; set; }

            /// <summary>
            /// builds a 2d array the represents the board from the data string
            /// </summary>
            /// <param name="data">a string that represents the board</param>
            public Board(string data)
            {
                _board = new char[8, 8];

                for (int i = 0; i < 8; i++)
                {
                    for (int j = 0; j < 8; j++)
                    {
                        _board[j, i] = data[i * 8 + j];
                    }
                }
            }

            /// <summary>
            /// get the char in the desired block
            /// </summary>
            /// <param name="dst">the desired block</param>
            /// <returns>the character in the block</returns>
            public char GetCharInDest(Point dst)
            {
                return _board[dst.X, dst.Y];
                //return _board[(dst[1] - '1'), (dst[0] - 'a')];
            }
        }

        /// <summary>
        /// constractor
        /// </summary>
        /// <param name="place">point that represents the place of the figure</param>
        /// <param name="boardData">string that contains all the board information</param>
        public Figure(Point place, string boardData)
        {
            _place = place;
            _board = new Board(boardData);
        }

        /// <summary>
        /// constractor
        /// </summary>
        /// <param name="place">point that represents the place of the figure</param>
        /// <param name="board">class that contains all the board information</param>
        public Figure(Point place, Board board)
        {
            _place = place;
            _board = board;
        }

        /// <summary>
        /// checks if the move can be made with the current figure on the board in the current situation
        /// </summary>
        /// <param name="dst">the place the figure want to end up at</param>
        /// <returns>number that defines the error or if the move is valid</returns>
        public virtual int CheckMove(Point dst)
        {
            //char figureInDest = _board.GetCharInDest(dst);  //the figure in the destination

            /*if (_place == dst)  //the figure is not moving
            {
                return SAME_PLACE;
            }*/ //the client can do that
            /*if ((figureInDest > 'a' && figureInDest < 'z') == (_type > 'a' && _type < 'z')) //ally figure in the dest
            {
                return ALLY_FIGURE_IN_DST;
            }*/ //the client can do that
            if (ValidMove(dst)) //the figure can move this way
            {
                if (!IsWayClear(dst))   //there is a figure in the way
                {
                    return FIGURES_IN_THE_WAY;
                }
            }
            else
            {
                return INVALID_MOVE_FOR_FIGURE;
            }

            return OK;  //valid move
        }

        /// <summary>
        /// override to match for current figure movement
        /// </summary>
        /// <param name="dst">the place the figure want to end up at</param>
        /// <returns>if the move logicly valid</returns>
        public virtual bool ValidMove(Point dst)
        {
            return true;
        }

        /// <summary>
        /// override to match for current figure movement
        /// </summary>
        /// <param name="dst">the place the figure want to end up at</param>
        /// <returns>if nobody is blocking the figure</returns>
        public virtual bool IsWayClear(Point dst)
        {
            return true;
        }
    }
}