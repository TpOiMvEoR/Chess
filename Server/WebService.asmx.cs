﻿using Server;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web.Services;

namespace DB_with_WEB_SERVICES
{
    /// <summary>
    /// Summary description for WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {
        /// <summary>
        /// test
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        /// <summary>
        /// login
        /// </summary>
        /// <param name="username">the username to login with</param>
        /// <param name="password">the password to the username</param>
        /// <returns>if a user with those 2 params exist</returns>
        [WebMethod]
        public bool LogIn(string username, string password)
        {
            Connection cn = new Connection();
            string sql = $"SELECT Username, Password FROM Users WHERE Username like '{username}'";
            SqlDataReader r = cn.getDataReader2(sql);
            while (r.Read())
                if ((r["Username"].ToString() == username) && r["Password"].ToString() == password)
                    return true;
            return false;
        }

        /// <summary>
        /// register
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="email"></param>
        /// <param name="img"></param>
        /// <returns>if there is no username like the arg in the db</returns>
        [WebMethod]
        public bool Register(string firstName, string lastName, string username, string password, string email, string img)
        {
            Connection cn = new Connection();
            string sql = $"SELECT Username FROM Users WHERE Username like '{username}';";
            SqlDataReader r = cn.getDataReader2(sql);
            while (r.Read())
                if (r["Username"].ToString() == username)
                    return false;
            cn = new Connection();
            sql = $"INSERT INTO Users (First_Name, Last_Name, Username, Password, Mail, Profile_Pic) VALUES ('{firstName}', '{lastName}', '{username}', '{password}', '{email}', '{img}');";
            cn.Insert(sql);
            return true;
        }

        /// <summary>
        /// update information of the user (whole row)
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="username_new"></param>
        /// <param name="username_old"></param>
        /// <param name="password"></param>
        /// <param name="email"></param>
        /// <param name="img"></param>
        /// <returns>if the new username is the same as the old or doesn't appear in the db and the db was updated</returns>
        [WebMethod]
        public bool UpdeteInfo(string firstName, string lastName, string username_new, string username_old, string password, string email, string img)
        {
            int id = 13;
            Connection cn = new Connection();
            string sql = $"SELECT ID, Username FROM Users WHERE Username like '{username_old}';";
            SqlDataReader r = cn.getDataReader2(sql);
            while (r.Read())
                if (r["Username"].ToString() == username_old)
                {
                    id = (int)r["ID"];
                    break;
                }
            if (username_new != username_old)   //if it is the same username
            {
                sql = $"SELECT ID, Username FROM Users WHERE Username like '{username_new}';";
                cn = new Connection();
                SqlDataReader rn = cn.getDataReader2(sql);
                while (rn.Read())
                    if (rn["Username"].ToString() == username_new)
                        return false;
            }
            sql = $"UPDATE Users SET First_Name = '{firstName}', Last_Name = '{lastName}', Username = '{username_new}', Password = '{password}', Mail = '{email}'";
            if (img.Length != 0)
            {
                sql += $", Profile_Pic = '{img}'";
            }
            sql += $" WHERE ID = {id};";
            cn = new Connection();
            cn.Insert(sql); //stuck here
            //cn.Insert($"Update Users SET First_Name='momo' WHERE ID=11;");
            return true;
        }

        [Serializable]
        public class UserInfo
        {
            public string first_name { get; set; }
            public string last_name { get; set; }
            public string username { get; set; }
            public string password { get; set; }
            public string email { get; set; }
            public string picture { get; set; }
        }

        /// <summary>
        /// get information about the user
        /// </summary>
        /// <param name="username">the username</param>
        /// <returns></returns>
        [WebMethod]
        public UserInfo GetUserInfo(string username)
        {
            //UserInfo userInfo = new UserInfo();
            UserInfo userInfo = new UserInfo();
            Connection cn = new Connection();
            string sql = $"SELECT ID, Username FROM Users WHERE Username like '{username}';";
            SqlDataReader r = cn.getDataReader2(sql);
            while (r.Read())
                if (r["Username"].ToString() == username)
                {
                    sql = $"SELECT First_Name, Last_Name, Username, Password, Mail, Profile_Pic FROM Users WHERE ID = {r["ID"]};";
                    cn = new Connection();
                    SqlDataReader r2 = cn.getDataReader2(sql);
                    r2.Read();
                    userInfo = new UserInfo
                    {
                        first_name = r2["First_Name"].ToString(),
                        last_name = r2["Last_Name"].ToString(),
                        username = r2["Username"].ToString(),
                        password = r2["Password"].ToString(),
                        email = r2["Mail"].ToString(),
                        picture = r2["Profile_Pic"].ToString()
                    };
                    /*userInfo =
                        r2["First_Name"].ToString() + "!" +
                        r2["Last_Name"].ToString() + "!" +
                        r2["Username"].ToString() + "!" +
                        r2["Password"].ToString() + "!" +
                        r2["Mail"].ToString() + "!";*/
                    //r["Profile_Pic"].ToString();

                    cn.ConnectionClose();

                    break;
                }
            return userInfo;
        }

        /// <summary>
        /// get the image of the user
        /// </summary>
        /// <param name="username">the username</param>
        /// <returns>the image as byte array</returns>
        [WebMethod]
        public byte[] GetUserImg(string username)
        {
            string hex = "";
            byte[] byteImg = { };
            Connection cn = new Connection();
            string sql = $"SELECT Username, Profile_Pic FROM Users WHERE Username like '{username}';";
            SqlDataReader r = cn.getDataReader2(sql);
            while (r.Read())
            {
                if (r["Username"].ToString() == username)
                {
                    hex = r["Profile_Pic"].ToString();
                    byteImg = Enumerable.Range(0, hex.Length).Where(x => x % 2 == 0).Select(x => Convert.ToByte(hex.Substring(x, 2), 16)).ToArray();
                    break;
                }
            }
            return byteImg;
        }

        /// <summary>
        /// saves the game
        /// </summary>
        /// <param name="username">username</param>
        /// <param name="data">string that represent the board</param>
        [WebMethod]
        public void SaveGame(string username, string data)
        {
            Connection cn = new Connection();
            string sql = $"SELECT ID, Username FROM Users WHERE Username like '{username}'";
            SqlDataReader r = cn.getDataReader2(sql);
            while (r.Read())
                if (r["Username"].ToString() == username)
                {
                    cn = new Connection();
                    sql = $"SELECT * FROM Boards WHERE User_ID = {r["ID"]}";
                    SqlDataReader r2 = cn.getDataReader2(sql);
                    if (r2.Read())
                    {
                        sql = $"UPDATE Boards SET Board = '{data}' WHERE User_ID = {r["ID"]}";
                    }
                    else
                    {
                        sql = $"INSERT INTO Boards VALUES({r["ID"]}, '{data}')";
                    }
                    cn = new Connection();
                    r2 = cn.getDataReader2(sql);
                }
        }

        /// <summary>
        /// get users' saved board
        /// </summary>
        /// <param name="username">username</param>
        /// <returns>the board as string</returns>
        [WebMethod]
        public string GetSavedBoard(string username)
        {
            string boards = "";
            int userId = 0;
            Connection cn = new Connection();
            string sql = $"SELECT ID, Username FROM Users WHERE Username Like '{username}'";
            SqlDataReader r1 = cn.getDataReader2(sql);

            while (r1.Read())
            {
                if (r1["Username"].ToString() == username)
                {
                    userId = (int)r1["ID"];
                    break;
                }
            }

            sql = $"SELECT Board FROM Boards WHERE User_ID = {userId}";
            cn = new Connection();
            r1 = cn.getDataReader2(sql);

            if (r1.Read())
            {
                boards = r1["Board"].ToString();
            }

            return boards;
        }

        /*[WebMethod]
        public string GetBoardData(int boardId)
        {
            Connection cn = new Connection();
            string sql = $"SELECT Board FROM Boards WHERE ID = {boardId}";
            SqlDataReader r = cn.getDataReader2(sql);

            r.Read();
            string data = r["Board"].ToString();

            return data;
        }*/

        /*        Pawn,
                Rook,
                Knight,
                Bishop,
                Queen,
                King*/

        /// <summary>
        /// check algorithems of movement and game logic
        /// </summary>
        /// <param name="place">the location of the piece that want to move</param>
        /// <param name="data">the board as a string</param>
        /// <param name="pieceType">the type of the piece by the enum in game.cs (client)</param>
        /// <param name="dest">the place the piece want to be at by the end of the move</param>
        /// <returns>if the move is valid for the figure</returns>
        [WebMethod]
        public int CheckMove(string place, string data, int pieceType, string dest)
        {
            int id = 0;
            Figure figure = null;
            Point _dest = new Point((int)char.GetNumericValue(dest[0]), (int)char.GetNumericValue(dest[1]));
            Point _place = new Point((int)char.GetNumericValue(place[0]), (int)char.GetNumericValue(place[1]));

            switch (pieceType)
            {
                case 0:
                    {
                        figure = new Pawn(_place, data);
                        break;
                    }

                case 1:
                    {
                        figure = new Rook(_place, data);
                        break;
                    }
                case 2:
                    {
                        figure = new Knight(_place, data);
                        break;
                    }
                case 3:
                    {
                        figure = new Bishop(_place, data);
                        break;
                    }
                case 4:
                    {
                        figure = new Queen(_place, data);
                        break;
                    }
                case 5:
                    {
                        figure = new King(_place, data);
                        break;
                    }
            }

            id = figure.CheckMove(_dest);


            return id;
        }

        /// <summary>
        /// checks if there is a check on the king in "place"
        /// </summary>
        /// <param name="place">the placement of the king</param>
        /// <param name="data">the board as a string</param>
        /// <returns>if a check can be caused in the coard current situation</returns>
        [WebMethod]
        public bool IsCheck(string place, string data)
        {
            Point p = new Point((int)char.GetNumericValue(place[0]), (int)char.GetNumericValue(place[1]));
            King k = new King(p, data);

            return k.IsCheck();
        }

        /*
   [WebMethod]

   public int NewUser(int age, string pob, string username, string password)
   {
       Connection cn = new Connection();
       string sql = "INSERT INTO userdata (age, placeofbirth, username, password) VALUES('" + age + "', '" + pob + "', '" + username + "', '" + password + "')";
       cn.Insert(sql);
       string sql3 = "SELECT teamID FROM userdata where username='" + username + "' AND password='" + password + "'";
       SqlDataAdapter ad = cn.GetDataAdapter(sql3);
       string tid = cn.Getitem(ad, "teamID", 0);
       string sql2 = "INSERT INTO wallet (userID,money,valueplayers,Tokens) VALUES('" + tid + "', '" + "1000" + "', '" + "0" + "', '" + "5" + "')";
       return cn.Insert(sql2);
   }
}
        */
        //Model class for data grid in admin screen
        [Serializable]
        public class UserData
        {
            public string _firstName { get; set; }
            public string _lastName { get; set; }
            public string _username { get; set; }
            public string _password { get; set; }
            public string _email { get; set; }
            //public string _image { get; set; }
            public string _boardStr { get; set; }
            public int _id { get; set; }
        }

        /// <summary>
        /// View Model that gets all the users with their boards even if user doesn't have a board
        /// </summary>
        /// <returns>a list of the class above for the have all the users in the db</returns>
        [WebMethod]
        public List<UserData> GetAllUsersWithBoards()
        {
            List<UserData> userDatas = new List<UserData>();
            Connection cn = new Connection();
            string sql = $"SELECT Users.ID, First_Name, Last_Name, Username, Password, Mail, Boards.Board FROM Users LEFT JOIN Boards ON Users.ID = User_ID;";
            SqlDataReader r = cn.getDataReader2(sql);
            while (r.Read())
            {
                userDatas.Add(new UserData
                {
                    _firstName = r["First_Name"].ToString(),
                    _lastName = r["Last_Name"].ToString(),
                    _username = r["Username"].ToString(),
                    _password = r["Password"].ToString(),
                    _email = r["Mail"].ToString(),
                    _boardStr = r["Board"].ToString(),
                    _id = (int)r["ID"]
                });
            }

            return userDatas;
        }

        /// <summary>
        /// checks if the user is an admin
        /// </summary>
        /// <param name="username">the username</param>
        /// <returns>if the username is an admin</returns>
        [WebMethod]
        public bool IsAdmin(string username)
        {
            Connection cn = new Connection();
            string sql = $"SELECT Username, Is_Admin FROM Users Where Username LIKE '{username}';";
            SqlDataReader r = cn.getDataReader2(sql);

            while (r.Read())
            {
                if (r["Username"].ToString() == username)
                {
                    return (bool)r["Is_Admin"];
                }
            }

            return false;
        }

        /// <summary>
        /// updates the information that was changed in the admin page
        /// </summary>
        /// <param name="userData">class that contains all the needed data</param>
        /// <returns>if the update succeeded</returns>
        [WebMethod]
        public bool Update(UserData userData)
        {
            Connection cn = new Connection();
            string sql = $"Select Username FROM Users WHERE ID = {userData._id};";
            SqlDataReader r = cn.getDataReader2(sql);
            r.Read();
            bool updeted = UpdeteInfo(userData._firstName, userData._lastName, userData._username, r["Username"].ToString(), userData._password, userData._email, "");

            if (updeted && userData._boardStr.Length > 0)  //client checks validity, we only check if we got a board and not empty string
            {
                SaveGame(userData._username, userData._boardStr);
                return true;
            }

            return updeted;
        }

        /// <summary>
        /// delete the user and its' board (info from admin)
        /// </summary>
        /// <param name="id">the id os the user to delete</param>
        [WebMethod]
        public void Delete(int id)
        {
            Connection cn;
            string sql;
            //SqlDataReader r;

            sql = $"DELETE Boards WHERE User_ID = {id};";   //v check if throws exception for user with no board [working for me]
            cn = new Connection();
            cn.Insert(sql);

            //can not make deleting from 2 tables at once work
            sql = $"DELETE Users WHERE ID = {id};";
            cn = new Connection();
            cn.Insert(sql);

        }
    }
}