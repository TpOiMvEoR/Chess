﻿using System;
using System.Drawing;

namespace Server
{
    //isWayClear function can be used in other classes, therefore they inherit from it
    public class Bishop : Figure
    {
        private int _up = 0;
        private int _right = 0;

        /// <summary>
        /// constractor
        /// </summary>
        /// <param name="place">point that represents the place of the figure</param>
        /// <param name="data">string that contains all the board information</param>
        public Bishop(Point place, string data)
            : base(place, data)
        {
        }

        /// <summary>
        /// constractor
        /// </summary>
        /// <param name="place">point that represents the place of the figure</param>
        /// <param name="board">class that contains all the board information</param>
        public Bishop(Point place, Board board)
            : base(place, board)
        {
        }

        /// <summary>
        /// override to match for current figure movement
        /// </summary>
        /// <param name="dst">the place the figure want to end up at</param>
        /// <returns>if the move logicly valid</returns>
        public override bool ValidMove(Point dst)   //clear map move validity
        {
            return Math.Abs(_place.X - dst.X) == Math.Abs(_place.Y - dst.Y); //moves only right with up
        }

        /// <summary>
        /// override to match for current figure movement
        /// </summary>
        /// <param name="dst">the place the figure want to end up at</param>
        /// <returns>if nobody is blocking the figure</returns>
        public override bool IsWayClear(Point dst)
        {
            Point temp = _place;
            /*int re0;
            int re1;*/

            if (_place.X != dst.X)
            {
                _right = (_place.X - dst.X < 0) ? 1 : -1;
            }
            if (_place.Y != dst.Y)
            {
                _up = (_place.Y - dst.Y < 0) ? 1 : -1;
            }
            /*re0 = (char)((int)temp[0] + _right);
            re1 = (char)((int)temp[1] + _up );
            temp = re0.ToString() + re1.ToString();*/
            temp.X += _right;
            temp.Y += _up;

            while (temp != dst)
            {
                if (_board.GetCharInDest(temp) != '#')
                {
                    return false;
                }
                temp.X += _right;
                temp.Y += _up;
                /*re0 = (char)((int)temp[0] + _right);
                re1 = (char)((int)temp[1] + _up);
                temp = re0.ToString() + re1.ToString();*/
            }
            return true;
        }
    }
}