﻿using System.Drawing;

namespace Server
{
    //is a combination of rook and bishop
    public class Queen : Bishop
    {
        /// <summary>
        /// constractor
        /// </summary>
        /// <param name="place">point that represents the place of the figure</param>
        /// <param name="data">string that contains all the board information</param>
        public Queen(Point place, string data)
            : base(place, data)
        {
        }

        /// <summary>
        /// constractor
        /// </summary>
        /// <param name="place">point that represents the place of the figure</param>
        /// <param name="board">class that contains all the board information</param>
        public Queen(Point place, Board board)
            : base(place, board)
        {
        }

        /// <summary>
        /// override to match for current figure movement
        /// </summary>
        /// <param name="dst">the place the figure want to end up at</param>
        /// <returns>if the move logicly valid</returns>
        public override bool ValidMove(Point dst)   //clear map move validity
        {
            Rook r = new Rook(_place, _board);
            Bishop b = new Bishop(_place, _board);

            return r.ValidMove(dst) || b.ValidMove(dst);
        }
    }
}