﻿using System;
using System.Drawing;

namespace Server
{
    //jumps over figures; no need to check if the way is clear
    public class Knight : Figure
    {
        /// <summary>
        /// constractor
        /// </summary>
        /// <param name="place">point that represents the place of the figure</param>
        /// <param name="data">string that contains all the board information</param>
        public Knight(Point place, string data)
            : base(place, data)
        {
        }

        /// <summary>
        /// constractor
        /// </summary>
        /// <param name="place">point that represents the place of the figure</param>
        /// <param name="board">class that contains all the board information</param>
        public Knight(Point place, Board board)
            : base(place, board)
        {
        }

        /// <summary>
        /// override to match for current figure movement
        /// </summary>
        /// <param name="dst">the place the figure want to end up at</param>
        /// <returns>if the move logicly valid</returns>
        public override bool ValidMove(Point dst)   //clear map move validity
        {
            if (Math.Abs(_place.X - dst.X) == 2)
            {
                if (Math.Abs(_place.Y - dst.Y) == 1)
                    return true;
            }
            else if (Math.Abs(_place.X - dst.X) == 1)
            {
                if (Math.Abs(_place.Y - dst.Y) == 2)
                    return true;
            }

            return false;
        }
    }
}