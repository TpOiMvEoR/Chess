﻿using System;
using System.Drawing;

namespace Server
{
    //cant move where it can be eaten
    public class King : Figure
    {
        /// <summary>
        /// constractor
        /// </summary>
        /// <param name="place">point that represents the place of the figure</param>
        /// <param name="data">string that contains all the board information</param>
        public King(Point place, string data)
            : base(place, data)
        {
        }

        /// <summary>
        /// constractor
        /// </summary>
        /// <param name="place">point that represents the place of the figure</param>
        /// <param name="board">class that contains all the board information</param>
        public King(Point place, Board board)
            : base(place, board)
        {
        }

        /// <summary>
        /// override to match for current figure movement
        /// </summary>
        /// <param name="dst">the place the figure want to end up at</param>
        /// <returns>if the move logicly valid</returns>
        public override bool ValidMove(Point dst)   //clear map move validity
        {
            return Math.Abs(_place.X - dst.X) <= 1 && Math.Abs(_place.Y - dst.Y) <= 1;
            /*if (!movement)
                return false;

            char c = _board.GetCharInDest(_place);
            Board b = _board;
            b._board[_place.X, _place.Y] = '#';
            b._board[dst.X, dst.Y] = c;
            King k = new King(dst, b);
            bool check = k.IsCheck();

            return movement && check;*/
        }

        /// <summary>
        /// ckeck if there is currently a check on the king in the fiven location
        /// </summary>
        /// <returns>if there is a check</returns>
        public bool IsCheck()   //on the current king
        {
            Point p;
            bool black = _board.GetCharInDest(_place) < 'a';
            char c;
            char lower;
            Figure checker = new Figure(_place, _board);

            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    p = new Point(j, i);
                    c = _board.GetCharInDest(p);
                    if (c != '#')
                    {
                        if ((c < 'a') == !black) //check if opposit color
                        {
                            lower = char.ToLower(c);

                            /*      Pawn,
                                    Rook,
                                    Knight,
                                    Bishop,
                                    Queen,
                                    King*/

                            switch (lower)
                            {
                                case 'p':
                                    {
                                        checker = new Pawn(p, _board);
                                        break;
                                    }

                                case 'r':
                                    {
                                        checker = new Rook(p, _board);
                                        break;
                                    }
                                case 'n':
                                    {
                                        checker = new Knight(p, _board);
                                        break;
                                    }
                                case 'b':
                                    {
                                        checker = new Bishop(p, _board);
                                        break;
                                    }
                                case 'q':
                                    {
                                        checker = new Queen(p, _board);
                                        break;
                                    }
                                case 'k':
                                    {
                                        checker = new King(p, _board);
                                        break;
                                    }
                            }

                            if (checker.CheckMove(_place) == 0)
                            {
                                return true;
                            }
                        }
                    }
                }
            }

            return false;
        }
    }
}