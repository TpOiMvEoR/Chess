﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Chess
{
    //enums for chess piece
    public enum PieceType
    {
        Pawn,
        Rook,
        Knight,
        Bishop,
        Queen,
        King
    }

    public enum Player
    {
        White,
        Black
    }

    public enum Alive
    {
        Yes,
        No
    }

    /// <summary>
    /// class that represent every piece on the board
    /// </summary>
    public class ChessPiece : ViewModelBase
    {
        private Point _Pos;
        public Point Pos
        {
            get { return new Point(this._Pos.X, this._Pos.Y); }
            set { this._Pos = value; RaisePropertyChanged(() => this.Pos); }
        }

        private PieceType _Type;
        public PieceType Type
        {
            get { return this._Type; }
            set { this._Type = value; RaisePropertyChanged(() => this.Type); }
        }

        private Player _Player;
        public Player Player
        {
            get { return this._Player; }
            set { this._Player = value; RaisePropertyChanged(() => this.Player); }
        }

        private Alive _Alive;
        public Alive Alive
        {
            get { return this._Alive; }
            set { this._Alive = value; RaisePropertyChanged(() => this.Alive); }
        }

    }


    /// <summary>
    /// Interaction logic for Game.xaml
    /// </summary>
    public partial class Game : Window
    {
        private string _username;
        private bool _whitesTurn = true;
        private Movement _m = new Movement();
        private string _wrong;
        private List<ChessPiece> _pieces;
        private ChessPiece black_king;
        private ChessPiece white_king;

        private readonly db_server.WebService1SoapClient c = new db_server.WebService1SoapClient();

        /// <summary>
        /// class for every movement in game
        /// </summary>
        private class Movement
        {
            public Point _source { get; set; }
            public Point _destination { get; set; }
            public bool _gotSource { get; set; }
            public bool _gotDest { get; set; }
            public int _pieceType { get; set; }
            public ChessPiece _piecePointer { get; set; }

            /// <summary>
            /// constractor
            /// </summary>
            public Movement()
            {
                _gotSource = false;
                _gotDest = false;
            }
        }

        /// <summary>
        /// dict for easier transition from data string to chess pieces
        /// </summary>
        public Dictionary<char, PieceType> dict = new Dictionary<char, PieceType>
        {
            { 'p', PieceType.Pawn },
            { 'r', PieceType.Rook },
            { 'n', PieceType.Knight },
            { 'b', PieceType.Bishop },
            { 'q', PieceType.Queen },
            { 'k', PieceType.King }
        };

        /// <summary>
        /// constractor, the board is 400x400 so every block is 50x50, therefore every block value is multiplied by 50
        /// </summary>
        /// <param name="username">the username of the user</param>
        /// <param name="gamedata">the data string that represents the board</param>
        public Game(string username, string gamedata)
        {
            InitializeComponent();
            _username = username;


            _pieces = new ObservableCollection<ChessPiece>  //setting the board
            {
                new ChessPiece{Pos=new Point(0, 6*50), Type=PieceType.Pawn, Player=Player.White, Alive=Alive.Yes},      //0
                new ChessPiece{Pos=new Point(1*50, 6*50), Type=PieceType.Pawn, Player=Player.White, Alive=Alive.Yes},   //1
                new ChessPiece{Pos=new Point(2*50, 6*50), Type=PieceType.Pawn, Player=Player.White, Alive=Alive.Yes},   //2
                new ChessPiece{Pos=new Point(3*50, 6*50), Type=PieceType.Pawn, Player=Player.White, Alive=Alive.Yes},   //3
                new ChessPiece{Pos=new Point(4*50, 6*50), Type=PieceType.Pawn, Player=Player.White, Alive=Alive.Yes},   //4
                new ChessPiece{Pos=new Point(5*50, 6*50), Type=PieceType.Pawn, Player=Player.White, Alive=Alive.Yes},   //5
                new ChessPiece{Pos=new Point(6*50, 6*50), Type=PieceType.Pawn, Player=Player.White, Alive=Alive.Yes},   //6
                new ChessPiece{Pos=new Point(7*50, 6*50), Type=PieceType.Pawn, Player=Player.White, Alive=Alive.Yes},   //7
                new ChessPiece{Pos=new Point(0*50, 7*50), Type=PieceType.Rook, Player=Player.White, Alive=Alive.Yes},   //8
                new ChessPiece{Pos=new Point(1*50, 7*50), Type=PieceType.Knight, Player=Player.White, Alive=Alive.Yes}, //9
                new ChessPiece{Pos=new Point(2*50, 7*50), Type=PieceType.Bishop, Player=Player.White, Alive=Alive.Yes}, //10
                new ChessPiece{Pos=new Point(3*50, 7*50), Type=PieceType.King, Player=Player.White, Alive=Alive.Yes},   //11 *
                new ChessPiece{Pos=new Point(4*50, 7*50), Type=PieceType.Queen, Player=Player.White, Alive=Alive.Yes},  //12
                new ChessPiece{Pos=new Point(5*50, 7*50), Type=PieceType.Bishop, Player=Player.White, Alive=Alive.Yes}, //13
                new ChessPiece{Pos=new Point(6*50, 7*50), Type=PieceType.Knight, Player=Player.White, Alive=Alive.Yes}, //14
                new ChessPiece{Pos=new Point(7*50, 7*50), Type=PieceType.Rook, Player=Player.White, Alive=Alive.Yes},   //15
                new ChessPiece{Pos=new Point(0*50, 1*50), Type=PieceType.Pawn, Player=Player.Black, Alive=Alive.Yes},   //16
                new ChessPiece{Pos=new Point(1*50, 1*50), Type=PieceType.Pawn, Player=Player.Black, Alive=Alive.Yes},   //17
                new ChessPiece{Pos=new Point(2*50, 1*50), Type=PieceType.Pawn, Player=Player.Black, Alive=Alive.Yes},   //18
                new ChessPiece{Pos=new Point(3*50, 1*50), Type=PieceType.Pawn, Player=Player.Black, Alive=Alive.Yes},   //19
                new ChessPiece{Pos=new Point(4*50, 1*50), Type=PieceType.Pawn, Player=Player.Black, Alive=Alive.Yes},   //20
                new ChessPiece{Pos=new Point(5*50, 1*50), Type=PieceType.Pawn, Player=Player.Black, Alive=Alive.Yes},   //21
                new ChessPiece{Pos=new Point(6*50, 1*50), Type=PieceType.Pawn, Player=Player.Black, Alive=Alive.Yes},   //22
                new ChessPiece{Pos=new Point(7*50, 1*50), Type=PieceType.Pawn, Player=Player.Black, Alive=Alive.Yes},   //23
                new ChessPiece{Pos=new Point(0*50, 0*50), Type=PieceType.Rook, Player=Player.Black, Alive=Alive.Yes},   //24
                new ChessPiece{Pos=new Point(1*50, 0*50), Type=PieceType.Knight, Player=Player.Black, Alive=Alive.Yes}, //25
                new ChessPiece{Pos=new Point(2*50, 0*50), Type=PieceType.Bishop, Player=Player.Black, Alive=Alive.Yes}, //26
                new ChessPiece{Pos=new Point(3*50, 0*50), Type=PieceType.King, Player=Player.Black, Alive=Alive.Yes},   //27 *
                new ChessPiece{Pos=new Point(4*50, 0*50), Type=PieceType.Queen, Player=Player.Black, Alive=Alive.Yes},  //28
                new ChessPiece{Pos=new Point(5*50, 0*50), Type=PieceType.Bishop, Player=Player.Black, Alive=Alive.Yes}, //29
                new ChessPiece{Pos=new Point(6*50, 0*50), Type=PieceType.Knight, Player=Player.Black, Alive=Alive.Yes}, //30
                new ChessPiece{Pos=new Point(7*50, 0*50), Type=PieceType.Rook, Player=Player.Black, Alive=Alive.Yes}    //31
            }.ToList();

            white_king = _pieces[11];
            black_king = _pieces[27];

            //this.ChessBoard.ItemsSource.Cast<ObservableCollection<ChessPiece>>().ToList()[;


            if (gamedata.Length > 0)    //if this arg is not empty we should set the game as written in the data stirng
            {
                /*string data = c.GetSavedBoard(_username);*/
                Continue(gamedata);
            }

            this.ChessBoard.ItemsSource = _pieces;
        }

        /// <summary>
        /// set the game as it was saved
        /// </summary>
        /// <param name="data">the string that represents the saved game</param>
        private void Continue(string data)
        {
            ObservableCollection<ChessPiece> p = new ObservableCollection<ChessPiece>();
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (data[(i * 8) + j] != '#')
                    {
                        p.Add(new ChessPiece { Pos = new Point(j * 50, i * 50), Type = dict[char.ToLower(data[(i * 8) + j])], Player = data[(i * 8) + j] > 'a' ? Player.White : Player.Black, Alive = Alive.Yes });
                    }
                }
            }
            _pieces = p.ToList();
            _whitesTurn = data[data.Length - 1] == '1';
            if (_whitesTurn)
                turn.Content = "White's turn";
            else
                turn.Content = "Black's turn";
        }

        /// <summary>
        /// if a move is not correct we need to start the movement process again and get new source and destination
        /// </summary>
        private void ResetMove()
        {
            /*this.ChessBoard.ItemsSource = _pieces;*/

            _m._gotDest = false;
            _m._gotSource = false;
            /*for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    _labelArray[i, j].Foreground = new SolidColorBrush(Colors.Red);
                    if (_boardArray[i, j] != '#')
                    {
                        Image img = new Image();
                        img.Source = new BitmapImage(new Uri(_imgDict[_boardArray[i, j]], UriKind.Relative));
                        _labelArray[i, j].Content = img;
                    }
                    else
                    {
                        _labelArray[i, j].Content = null;
                    }
                }
                *//*Image img = new Image();
                img.Source = new BitmapImage(new Uri(imgDict['R'], UriKind.Relative));
                a1.Content = img;
                img.Source = new BitmapImage(new Uri(imgDict['r'], UriKind.Relative));
                a2.Content = img;*//*
            }*/
        }

        /// <summary>
        /// event handler that handles the movement at the event of mouse left button
        /// </summary>
        /// <param name="sender">the canvas</param>
        /// <param name="e"></param>
        private async void Canvas_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Point block = BlockOnCanvas(e.GetPosition(sender as Canvas));   //get the position of the mouse on the canvas
            bool found = false;
            bool isChecked = false;

            _wrong = "";

            if (!_m._gotSource) //need to get source location
            {
                foreach (ChessPiece piece in _pieces)
                {
                    if (found)  //if the block of the location contains a chess piece
                    {
                        break;
                    }
                    if (piece.Pos == block)
                    {
                        found = true;
                        if (piece.Player == (_whitesTurn ? Player.White : Player.Black))    //if the color coresponds to the turn
                        {
                            _m._source = block;
                            _m._gotSource = true;
                            _m._pieceType = (int)piece.Type;
                            _m._gotSource = true;
                            _m._piecePointer = piece;
                        }
                        else
                        {
                            _wrong = "NOT YOUR TURN!";
                        }
                    }
                }
                if (!found) //you cant ake your move without moving a piece
                {
                    _wrong = "NO PIECES AT THIS PLACE!";
                }
            }
            else    //we have a sourve location; now we are checking the desired destination
            {
                if (block == _m._source)    //moving to the same place doesn't count as a move
                {
                    _wrong = "This is not a move!";
                    ResetMove();
                }
                else
                {
                    int i = 0;
                    foreach (ChessPiece piece in _pieces)
                    {
                        if (piece.Pos == block) //if there is a piece in the destination
                        {
                            if (piece.Player == (_whitesTurn ? Player.White : Player.Black))    //if its an ally piece
                            {
                                found = true;
                                _wrong = "ALLY PIECE IN DEST!";
                                ResetMove();
                            }
                            else    //it's an enemy piece
                            {
                                isChecked = true;
                                /*string place = (_m._source.X / 50).ToString() + (_m._source.Y / 50).ToString();
                                string data = BoardToString();
                                int type = (int)_m._pieceType;
                                string dest = ((block.X / 50)).ToString() + (block.Y / 50).ToString();
                                db_server.CheckMoveResponse r = await c.CheckMoveAsync(place, data, type, dest);
                                if (r.Body.CheckMoveResult != 0)
                                {
                                    _wrong = "This move is not valid!";
                                    ResetMove();
                                }*/
                                bool temp = await Check(block); //check the move
                                if (temp)
                                {
                                    piece.Alive = Alive.No; //vark piece as dead
                                    _pieces.RemoveAt(i);    //remove from our list
                                    /*                                    _m._piecePointer.Pos = block;
                                                                        _m._gotDest = true;

                                                                        Point toCheck = _whitesTurn ? black_king.Pos : white_king.Pos;
                                                                        string p = (toCheck.X / 50).ToString() + (toCheck.Y / 50).ToString();
                                                                        db_server.IsCheckResponse r2 = await c.IsCheckAsync(p, BoardToString());
                                                                        if (r2.Body.IsCheckResult)
                                                                        {
                                                                            string checkedKing = !_whitesTurn ? "White" : "Black";
                                                                            MessageBox.Show($"Check on the {checkedKing} King");
                                                                        }*/
                                }
                            }
                            break;
                        }
                        i++;
                    }
                    if (_wrong == "" && !isChecked)
                    {
                        await Check(block);
                        /*string place = (_m._source.X / 50).ToString() + (_m._source.Y / 50).ToString();
                        string data = BoardToString();
                        int type = _m._pieceType;
                        string dest = (block.X / 50).ToString() + (block.Y / 50).ToString();
                        db_server.CheckMoveResponse r = await c.CheckMoveAsync(place, data, type, dest);
                        if (r.Body.CheckMoveResult != 0)
                        {
                            _wrong = "This move is not valid!";
                            ResetMove();
                        }
                        else
                        {
                            _m._piecePointer.Pos = block;
                            _m._gotDest = true;
                            Point toCheck = _whitesTurn ? black_king.Pos : white_king.Pos;
                            string p = (toCheck.X / 50).ToString() + (toCheck.Y / 50).ToString();
                            db_server.IsCheckResponse r2 = await c.IsCheckAsync(p, BoardToString());
                            if (r2.Body.IsCheckResult)
                            {
                                string checkedKing = !_whitesTurn ? "White" : "Black";
                                MessageBox.Show($"Check on the {checkedKing} King");
                            }
                        }*/
                    }

                }
            }

            if (_wrong == "")   //if everything is ok
            {
                if (_m._gotDest)    //if the move was ok
                {
                    ResetMove();
                    _whitesTurn = !_whitesTurn; //change turn
                    if (_whitesTurn)
                        turn.Content = "White's turn";
                    else
                        turn.Content = "Black's turn";
                }
            }
            else
            {
                await ShowWrongFor3Sec();
            }
        }

        /// <summary>
        /// checks for move validity, self check and enemy check
        /// </summary>
        /// <param name="block">the desired destination</param>
        /// <returns>if the move is ok</returns>
        private async Task<bool> Check(Point block)
        {
            string place = (_m._source.X / 50).ToString() + (_m._source.Y / 50).ToString();
            string data = BoardToString();
            int type = _m._pieceType;
            string dest = (block.X / 50).ToString() + (block.Y / 50).ToString();
            db_server.CheckMoveResponse r = await c.CheckMoveAsync(place, data, type, dest);
            if (r.Body.CheckMoveResult != 0)    //0 means move is ok
            {
                _wrong = "This move is not valid!";
                ResetMove();
                return false;
            }
            else
            {
                Point prevPos = _m._piecePointer.Pos;   //save the prev point in case of self check
                _m._piecePointer.Pos = block;
                _m._gotDest = true;

                //enemy king
                Point toCheck = _whitesTurn ? black_king.Pos : white_king.Pos;
                string p = (toCheck.X / 50).ToString() + (toCheck.Y / 50).ToString();
                db_server.IsCheckResponse r2 = await c.IsCheckAsync(p, BoardToString());
                if (r2.Body.IsCheckResult)
                {
                    string checkedKing = !_whitesTurn ? "White" : "Black";
                    MessageBox.Show($"Check on the {checkedKing} King");
                }

                //same for our king
                toCheck = !_whitesTurn ? black_king.Pos : white_king.Pos;
                p = (toCheck.X / 50).ToString() + (toCheck.Y / 50).ToString();
                r2 = await c.IsCheckAsync(p, BoardToString());
                if (r2.Body.IsCheckResult)
                {
                    _m._piecePointer.Pos = prevPos;
                    _wrong = "Can't make this move (you will lose :[ )";
                    ResetMove();
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// get the first pixel of the block that was clicked
        /// </summary>
        /// <param name="p">the place of the click</param>
        /// <returns>the srart of the block</returns>
        private Point BlockOnCanvas(Point p)
        {
            int x = -1, y = -1;
            bool foundX = false, foundY = false;

            for (int i = 0; i < 8 * 50 && !(foundX && foundY); i += 50)
            {
                if (p.X <= (i + 50) && !foundX)
                {
                    x = i;
                    foundX = true;
                }

                if (p.Y <= (i + 50) && !foundY)
                {
                    y = i;
                    foundY = true;
                }
            }

            return new Point(x, y);
        }

        /// <summary>
        /// transition the information of the chess pieces and turn into a string that represents the board
        /// </summary>
        /// <returns></returns>
        private string BoardToString()
        {
            string data = "";
            Point temp = new Point();
            Point[] pieces = new Point[_pieces.Count];

            for (int i = 0; i < _pieces.Count; i++)
            {
                pieces[i] = _pieces[i].Pos;
            }

            for (int i = 0; i < 8; i++)
            {
                temp.Y = i * 50;
                for (int j = 0; j < 8; j++)
                {
                    temp.X = j * 50;

                    if (pieces.Contains(temp))  //if there is a piece at this location
                    {
                        char c = '#';

                        switch ((int)_pieces[Array.IndexOf(pieces, temp)].Type)
                        {
                            case 0:
                                {
                                    c = 'p';
                                    break;
                                }
                            case 1:
                                {
                                    c = 'r';
                                    break;
                                }
                            case 2:
                                {
                                    c = 'n';
                                    break;
                                }
                            case 3:
                                {
                                    c = 'b';
                                    break;
                                }
                            case 4:
                                {
                                    c = 'q';
                                    break;
                                }
                            case 5:
                                {
                                    c = 'k';
                                    break;
                                }
                        }
                        if ((int)_pieces[Array.IndexOf(pieces, temp)].Player == 1)  //if it's a black piece
                        {
                            c = char.ToUpper(c);
                        }
                        data += c;
                    }
                    else    //empty block
                    {
                        data += "#";
                    }
                }
            }


            return data;
        }

        /// <summary>
        /// saves the game for the user
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void SaveGame(object sender, RoutedEventArgs e)
        {
            string data = BoardToString();

            data += _whitesTurn ? 1 : 0;    //add turn information

            await c.SaveGameAsync(_username, data);

            MessageBox.Show("Game Saved!");
        }

        /// <summary>
        /// window closing event handler
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosing(CancelEventArgs e)
        {
            SaveGame(null, null);
            Menu wnd = new Menu(_username);
            wnd.Show();
            base.OnClosing(e);
        }

        /*private async void MouseLeftButtonUpAction(object sender, MouseButtonEventArgs e)
        {
            Label caller = sender as Label;

            if (!m._gotSource)
            {
                m._source = caller.Name;
                m._gotSource = true;
            }
            else
            {
                m._destination = caller.Name;
                m._gotSource = false;

                int x = (int)m._source[1] - (int)'1';
                int y = (int)m._source[0] - (int)'a';
                char toMove = _boardArray[x, y];

                if ((toMove > 'A' && !_whitesTurn && toMove < 'Z') || (toMove < 'z' && _whitesTurn && toMove > 'a'))
                {
                    _boardArray[x, y] = '#';
                    _boardArray[(int)m._destination[1] - (int)'1', (int)m._destination[0] - (int)'a'] = toMove;
                    _whitesTurn = !_whitesTurn;
                    Bind();
                    if (_whitesTurn)
                        turn.Content = "White's Turn";
                    else
                        turn.Content = "Black's Turn";
                }
                else if (toMove == '#')
                {
                    _wrong = "NO PIECES AT THIS PLACE!";
                    await ShowWrongFor3Sec();
                }
                else
                {
                    _wrong = "NOT YOUR TURN!";
                    await ShowWrongFor3Sec();
                }
            }
        }
         */
        /// <summary>
        /// show the reason that the player cant make this move for 3 seconds
        /// </summary>
        /// <returns></returns>
        private async Task ShowWrongFor3Sec()
        {
            wrong.Text = _wrong;
            wrong.Visibility = Visibility.Visible;

            await Task.Run(() => Thread.Sleep(3000));

            wrong.Visibility = Visibility.Hidden;
        }

        /*private void a8_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            MessageBox.Show("double click");
        }*/
    }
}
