﻿using System.Windows;

namespace Chess
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly db_server.WebService1SoapClient c = new db_server.WebService1SoapClient();

        /// <summary>
        /// constractor
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            //db.createDbFile();
            //db.createDbConnection();
            //db.createTables();
        }

        /// <summary>
        /// buttin click event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Login_Click(object sender, RoutedEventArgs e)
        {
            if (Username.Text.Length > 0 && Password.Text.Length > 0)
            {
                //check with db
                db_server.LogInResponse r = await c.LogInAsync(Username.Text, Password.Text);
                if (r.Body.LogInResult)
                {
                    db_server.GetUserImgResponse r2;
                    r2 = await c.GetUserImgAsync(Username.Text);
                    Menu wnd = new Menu(Username.Text);
                    this.Close();
                    wnd.Show();
                }
                else
                {
                    MessageBox.Show("Username does not exist or wrong password!");
                }
            }
            else
            {
                MessageBox.Show("Please fill both username and password!");
            }
        }

        /// <summary>
        /// button click event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Register_Click(object sender, RoutedEventArgs e)
        {
            Register wnd = new Register();
            this.Close();
            wnd.Show();
        }
    }
}
