﻿using System;
using System.Data.SQLite;
using System.IO;
using System.Windows;

namespace Chess
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public class DbCreator
        {
            SQLiteConnection dbConnection;
            SQLiteCommand command;
            string sqlCommand;
            string dbPath = System.Environment.CurrentDirectory + "\\DB";
            string dbFilePath;
            public void createDbFile()
            {
                if (!string.IsNullOrEmpty(dbPath) && !Directory.Exists(dbPath))
                    Directory.CreateDirectory(dbPath);
                dbFilePath = dbPath + "\\DB.db";
                if (!System.IO.File.Exists(dbFilePath))
                {
                    SQLiteConnection.CreateFile(dbFilePath);
                }
            }

            public string createDbConnection()
            {
                string strCon = string.Format("Data Source={0};", dbFilePath);
                dbConnection = new SQLiteConnection(strCon);
                dbConnection.Open();
                command = dbConnection.CreateCommand();
                return strCon;
            }

            public void createTables()
            {
                if (!checkIfExist("Users"))
                {
                    sqlCommand = "CREATE TABLE Users(id INTEGER PRIMARY KEY AUTOINCREMENT, username STRING, password STRING, img BLOB);";
                    executeQuery(sqlCommand);
                }

            }

            public bool checkIfExist(string tableName)
            {
                command.CommandText = "SELECT name FROM sqlite_master WHERE name='" + tableName + "'";
                var result = command.ExecuteScalar();

                return result != null && result.ToString() == tableName ? true : false;
            }

            public void executeQuery(string sqlCommand)
            {
                SQLiteCommand triggerCommand = dbConnection.CreateCommand();
                triggerCommand.CommandText = sqlCommand;
                triggerCommand.ExecuteNonQuery();
            }

            public bool checkIfTableContainsData(string tableName)
            {
                command.CommandText = "SELECT count(*) FROM " + tableName;
                var result = command.ExecuteScalar();

                return Convert.ToInt32(result) > 0 ? true : false;
            }


            public void fillTable()
            {
                if (!checkIfTableContainsData("MY_TABLE"))
                {
                    sqlCommand = "insert into MY_TABLE (code_test_type) values (999)";
                    executeQuery(sqlCommand);
                }
            }
        }
    }
}
