﻿using Chess.db_server;
using Microsoft.Win32;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows;
using System.Windows.Media.Imaging;

namespace Chess
{
    /// <summary>
    /// Interaction logic for UserSettings.xaml
    /// </summary>
    public partial class UserSettings : Window
    {
        private readonly db_server.WebService1SoapClient c = new db_server.WebService1SoapClient();
        private string source = "";
        private string _uname;
        private UserInfo _userInfo = new UserInfo();

        /// <summary>
        /// constractor
        /// </summary>
        /// <param name="uname">the users' username</param>
        public UserSettings(string uname)
        {
            InitializeComponent();
            _uname = uname;
            LoadInfo();
        }

        /// <summary>
        /// load the user info that is stored in the db ('!' is used for deserialization)
        /// </summary>
        public async void LoadInfo()
        {
            db_server.GetUserInfoResponse r = await c.GetUserInfoAsync(_uname);
            _userInfo = r.Body.GetUserInfoResult;
            
            First_Name.Text = _userInfo.first_name;
            Last_Name.Text = _userInfo.last_name;
            Username.Text = _userInfo.username;
            Password.Text = _userInfo.password;
            Email.Text = _userInfo.email;

            /*int index = res.IndexOf('!');
            _userInfo.first_name = res.Substring(0, res.IndexOf('!', 0, 1));
            _userInfo.last_name = res.Substring(res.IndexOf('!', 0, 1), res.IndexOf('!', 0, 2));
            _userInfo.username = res.Substring(res.IndexOf('!', 0, 2), res.IndexOf('!', 0, 3));
            _userInfo.password = res.Substring(res.IndexOf('!', 0, 3), res.IndexOf('!', 0, 4));
            _userInfo.email = res.Substring(res.IndexOf('!', 0, 4), res.IndexOf('!', 0, 5));
            //_userInfo.picture = res.Substring(res.IndexOf('!', 0, 4) + 1);*/

            ByteArrayToImage();
            imgPhoto.Visibility = Visibility.Visible;
        }

        //function similar to register to load the image to the window
        private async void ByteArrayToImage()
        {
            /*Image returnImage = new Image();
            BitmapImage myBitmapImage = new BitmapImage();
            myBitmapImage.BeginInit();
            myBitmapImage.StreamSource = new MemoryStream(byteArrayIn);
            //myBitmapImage.DecodePixelWidth = 200;
            myBitmapImage.EndInit();
            returnImage.Source = myBitmapImage;
            */
            db_server.GetUserImgResponse r;
            r = await c.GetUserImgAsync(_uname);
            byte[] byteArrayIn = r.Body.GetUserImgResult;

            if (byteArrayIn == null || byteArrayIn.Length == 0) return;
            var image = new BitmapImage();
            using (var mem = new MemoryStream(byteArrayIn))
            {
                mem.Position = 0;
                image.BeginInit();
                image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.UriSource = null;
                image.StreamSource = mem;
                image.EndInit();
            }
            image.Freeze();
            imgPhoto.Source = image;
        }

        //similar to func in register to load a new image
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == true)
            {
                source = op.FileName;
                imgPhoto.Source = new BitmapImage(new Uri(op.FileName));
            }

        }

        /// <summary>
        /// update the information in the db reminding the register but its updating and not registering
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Update_Click(object sender, RoutedEventArgs e)
        {
            if (this.Username.Text.Length > 0
                && this.Password.Text.Length > 0
                && this.First_Name.Text.Length > 0
                && this.Last_Name.Text.Length > 0
                && this.Email.Text.Length > 0
                && this.imgPhoto.Source != null)
            {
                if (source.Length == 0 || new FileInfo(source).Length / 1024 <= 40)
                {
                    string img = "";
                    if (source.Length != 0)
                    {
                        System.Drawing.Image a = System.Drawing.Image.FromFile(source);
                        byte[] imgByte = converterDemo(a);
                        img = BitConverter.ToString(imgByte).Replace("-", "");
                    }
                    db_server.UpdeteInfoResponse r = await c.UpdeteInfoAsync(First_Name.Text, Last_Name.Text, Username.Text, _uname, Password.Text, Email.Text, img);
                    if (r.Body.UpdeteInfoResult)
                    {
                        _uname = Username.Text;
                        this.Close();
                    }
                    else
                        MessageBox.Show("Username already exists!");
                }
                else
                {
                    MessageBox.Show("Image file must not exceed 40KB!");
                }
            }
            else
            {
                MessageBox.Show("Please fill username and password, and upload an image!");
            }
        }

        //same function from regiter
        private byte[] converterDemo(System.Drawing.Image x)
        {
            ImageConverter _imageConverter = new ImageConverter();
            byte[] xByte = (byte[])_imageConverter.ConvertTo(x, typeof(byte[]));

            if (xByte == null || xByte.Length == 0) return null;
            var image = new BitmapImage();
            using (var mem = new MemoryStream(xByte))
            {
                mem.Position = 0;
                image.BeginInit();
                image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.UriSource = null;
                image.StreamSource = mem;
                image.EndInit();
            }
            image.Freeze();

            return xByte;
        }

        /// <summary>
        /// closing window event handler
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosing(CancelEventArgs e)
        {
            Menu wnd = new Menu(_uname);
            wnd.Show();
            base.OnClosing(e);
        }
    }
}
