﻿using Microsoft.Win32;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows;
using System.Windows.Media.Imaging;


namespace Chess
{
    /// <summary>
    /// Interaction logic for Register.xaml
    /// </summary>
    public partial class Register : Window
    {
        private readonly db_server.WebService1SoapClient c = new db_server.WebService1SoapClient();
        private string source;

        /// <summary>
        /// constractor
        /// </summary>
        public Register()
        {
            InitializeComponent();
        }

        /// <summary>
        /// load button click event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //open picture from file
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == true)
            {
                source = op.FileName;   //save file path
                imgPhoto.Source = new BitmapImage(new Uri(op.FileName));
            }

        }

        /// <summary>
        /// button click event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Register_Click(object sender, RoutedEventArgs e)
        {
            if (this.Username.Text.Length > 0
                && this.Password.Text.Length > 0
                && this.First_Name.Text.Length > 0
                && this.Last_Name.Text.Length > 0
                && this.Email.Text.Length > 0
                && this.imgPhoto.Source != null)
            {
                if (new FileInfo(source).Length / 1024 <= 35)   //the server can't handle big data chunks
                {
                    //get image as a string that represents a byte array
                    System.Drawing.Image a = System.Drawing.Image.FromFile(source);
                    byte[] imgByte = converterDemo(a);
                    string img = BitConverter.ToString(imgByte).Replace("-", "");
                    db_server.RegisterResponse r = await c.RegisterAsync(First_Name.Text, Last_Name.Text, Username.Text, Password.Text, Email.Text, img);
                    if (r.Body.RegisterResult)
                        this.Close();
                    else
                        MessageBox.Show("Username already exists!");
                }
                else
                {
                    MessageBox.Show("Image file must not exceed 35KB!");
                }
            }
            else
            {
                MessageBox.Show("Please fill username and password, and upload an image! ('!' is not allowed)");
            }
        }

        /// <summary>
        /// converting image to byte array
        /// </summary>
        /// <param name="x">object of drawing.image that contains the desired image</param>
        /// <returns>byre array that represents the image</returns>
        private byte[] converterDemo(System.Drawing.Image x)
        {
            ImageConverter _imageConverter = new ImageConverter();
            byte[] xByte = (byte[])_imageConverter.ConvertTo(x, typeof(byte[]));

            if (xByte == null || xByte.Length == 0) return null;
            var image = new BitmapImage();
            using (var mem = new MemoryStream(xByte))
            {
                mem.Position = 0;
                image.BeginInit();
                image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.UriSource = null;
                image.StreamSource = mem;
                image.EndInit();
            }
            image.Freeze();

            return xByte;
        }

        /// <summary>
        /// window closing event handler
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosing(CancelEventArgs e)
        {
            MainWindow wnd = new MainWindow();
            wnd.Show();
            base.OnClosing(e);
        }
    }
}
