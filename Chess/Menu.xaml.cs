﻿using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Media.Imaging;
using static Chess.ExternalServices;

namespace Chess
{
    /// <summary>
    /// Interaction logic for Menu.xaml
    /// </summary>
    public partial class Menu : Window
    {
        private string _uname;
        private bool returnToLogin = true;
        private readonly db_server.WebService1SoapClient c = new db_server.WebService1SoapClient();

        /// <summary>
        /// constractor
        /// </summary>
        /// <param name="uname">username of the user</param>
        public Menu(string uname)
        {
            InitializeComponent();
            _uname = uname;
            username.Content = uname;

            ByteArrayToImage(); //get the image from the db
            userImg.Visibility = Visibility.Visible;

            HebDate.Text = new HebrewDate().GetHebDate().ToString();
        }

        private async void ByteArrayToImage()
        {
            /*Image returnImage = new Image();
            BitmapImage myBitmapImage = new BitmapImage();
            myBitmapImage.BeginInit();
            myBitmapImage.StreamSource = new MemoryStream(byteArrayIn);
            //myBitmapImage.DecodePixelWidth = 200;
            myBitmapImage.EndInit();
            returnImage.Source = myBitmapImage;
            */
            db_server.GetUserImgResponse r;
            r = await c.GetUserImgAsync(_uname);
            byte[] byteArrayIn = r.Body.GetUserImgResult;

            if (byteArrayIn == null || byteArrayIn.Length == 0) return;
            //create an image object and insert the data
            var image = new BitmapImage();
            using (var mem = new MemoryStream(byteArrayIn))
            {
                mem.Position = 0;
                image.BeginInit();
                image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.UriSource = null;
                image.StreamSource = mem;
                image.EndInit();
            }
            image.Freeze();
            userImg.Source = image;
        }

        /// <summary>
        /// window closing event handler
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosing(CancelEventArgs e)
        {
            if (returnToLogin)
            {
                MainWindow wnd = new MainWindow();
                wnd.Show();
                base.OnClosing(e);
            }
        }

        /// <summary>
        /// button click event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void New_Game_Click(object sender, RoutedEventArgs e)
        {

            db_server.GetSavedBoardResponse r;
            r = await c.GetSavedBoardAsync(_uname);

            if (r.Body.GetSavedBoardResult.Length == 0 ||   //there isn't a saved game
                MessageBox.Show("You already have an unfinished game, do you want to delete it and start a new one?", "Warning", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                returnToLogin = false;

                Game wnd = new Game(_uname, "");
                this.Close();
                wnd.Show();
            }
        }

        /// <summary>
        /// button click event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Continue_Click(object sender, RoutedEventArgs e)
        {
            db_server.GetSavedBoardResponse r;
            r = await c.GetSavedBoardAsync(_uname);

            if (r.Body.GetSavedBoardResult.Length > 0)  //there is a saved game
            {
                returnToLogin = false;
                Game wnd = new Game(_uname, r.Body.GetSavedBoardResult);
                this.Close();
                wnd.Show();
            }
            else
            {
                MessageBox.Show("You don't have any unfinished games!");
            }
        }

        /// <summary>
        /// button click event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserSettings_Click(object sender, RoutedEventArgs e)
        {
            //open settings
            returnToLogin = false;
            UserSettings wnd = new UserSettings(_uname);
            this.Close();
            wnd.Show();
        }

        /// <summary>
        /// button click event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void AdminOptions_Click(object sender, RoutedEventArgs e)
        {
            //check if user is admin
            db_server.IsAdminResponse r = await c.IsAdminAsync(_uname);
            if (r.Body.IsAdminResult)   //if the user is an admin
            {
                returnToLogin = false;
                AdminWnd wnd = new AdminWnd();
                this.Close();
                wnd.Show();
            }
            else
            {
                MessageBox.Show("You are not an admin!\nYou don't have permitions for this option", "Error");
            }
        }

        /*private void Button_Click(object sender, RoutedEventArgs e)
        {

        }*/
    }
}
