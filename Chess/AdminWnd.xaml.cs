﻿using Chess.db_server;
using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;

namespace Chess
{
    /// <summary>
    /// Interaction logic for AdminWnd.xaml
    /// </summary>
    public partial class AdminWnd : Window
    {
        private readonly db_server.WebService1SoapClient c = new db_server.WebService1SoapClient();
        private UserData[] _userDatas;

        /// <summary>
        /// constractor
        /// </summary>
        /// <param name="uname"></param>
        public AdminWnd()
        {
            InitializeComponent();
            Init();
        }

        /// <summary>
        /// initialize the data grid
        /// </summary>
        private async void Init()
        {
            db_server.GetAllUsersWithBoardsResponse r = await c.GetAllUsersWithBoardsAsync();
            _userDatas = r.Body.GetAllUsersWithBoardsResult;
            data.ItemsSource = _userDatas;
        }

        /// <summary>
        /// closing event handler
        /// </summary>
        protected override void OnClosing(CancelEventArgs e)
        {
            MainWindow wnd = new MainWindow();
            wnd.Show();
            base.OnClosing(e);
        }

        /// <summary>
        /// delete selected row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Delete_Click(object sender, RoutedEventArgs e)
        {
            UserData ud = (UserData)data.SelectedItem;
            await c.DeleteAsync(ud._id);
            Refresh_Click(sender, e);
            MessageBox.Show("The selected user and his board were deleted");
        }

        /// <summary>
        /// save the selected row (checking validity of username and board)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Save_Click(object sender, RoutedEventArgs e)
        {
            UserData ud = (UserData)data.SelectedItem;
            db_server.UpdateResponse r;
            bool con = false;
            bool valid = true;

            if (ud._boardStr.Length == 65)
            {
                string allowed = "bBkKnNpPqQrR#";
                foreach (char c in ud._boardStr.Substring(0, 64))
                {
                    if (!allowed.Contains(c))
                    {
                        valid = false;
                        break;
                    }
                }
                if (ud._boardStr[64] != '1' && ud._boardStr[64] != '0')
                    valid = false;
            }
            else
                valid = false;

            if (ud._boardStr.Length == 0 || valid)
            {
                r = await c.UpdateAsync(ud);
                if (!r.Body.UpdateResult)
                    MessageBox.Show("Could not update row!\nThe new username you entered must be already taken", "Error");
                con = true;
            }

            if (!con)
                MessageBox.Show("Could not update row!\nThe board should be consisted of the figures first letter, capital for black, blank sqare is '#', each 8 characters is a row on the board starting from the top left corner;\n so there should be 64 characters of the board + '1' if whites turn, else '0'.", "Error");
            else
            {
                Refresh_Click(sender, e);
                MessageBox.Show("The selected row has been saved successfuly");
            }
        }

        private void Refresh_Click(object sender, RoutedEventArgs e)
        {
            Init();
        }
    }
}
