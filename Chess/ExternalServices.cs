﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Chess
{
    class ExternalServices
    {
        public class HebrewDate
        {
            public string year, month, day, hebStr;

            public HebrewDate()
            {
            }
            /*public HebrewDate(string year, string month, string day, string hebStr)
            {
                this.year = year;
                this.month = month;
                this.day = day;
                this.hebStr = hebStr;
            }*/

            public HebrewDate GetHebDate()
            {
                DateTime date = DateTime.Now;
                HebrewDate h = new HebrewDate();
                int y = date.Year, m = date.Month, d = date.Day;
                string url = $"https://www.hebcal.com/converter?cfg=xml&gy={y}&gm={m}&gd={d}&g2h=1";
                XmlDocument doc = new XmlDocument();
                doc.Load(url);
                XmlNode node = doc.SelectSingleNode("//hebrew ");
                h.year = node.Attributes["year"].Value;
                h.month = node.Attributes["month"].Value;
                h.day = node.Attributes["day"].Value;
                h.hebStr = node.Attributes["str"].Value;

                return h;
            }

            public static int Int(string s)
            {
                return int.Parse(s);
            }

            public override string ToString()
            {
                return $"{day} of {month}, {year} ({hebStr})";
            }
        }
    }
}
