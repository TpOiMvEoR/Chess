﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using javax.jws;

namespace DB_with_WEB_SERVICES
{
    /// <summary>
    /// Summary description for WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }
        [WebMethod]
        //התחברות לאפליקציה
        public bool LogIn(string username, string password)
        {
            Connection cn = new Connection();
            string sql = "SELECT * FROM [User]";
            SqlDataReader r = cn.getDataReader2(sql);
            while (r.Read())
                if ((r["UserName"].ToString() == username) && r["Password"].ToString() == password)
                    return true;
            return false;
        }
        /*
   [WebMethod]

   public int NewUser(int age, string pob, string username, string password)
   {
       Connection cn = new Connection();
       string sql = "INSERT INTO userdata (age, placeofbirth, username, password) VALUES('" + age + "', '" + pob + "', '" + username + "', '" + password + "')";
       cn.Insert(sql);
       string sql3 = "SELECT teamID FROM userdata where username='" + username + "' AND password='" + password + "'";
       SqlDataAdapter ad = cn.GetDataAdapter(sql3);
       string tid = cn.Getitem(ad, "teamID", 0);
       string sql2 = "INSERT INTO wallet (userID,money,valueplayers,Tokens) VALUES('" + tid + "', '" + "1000" + "', '" + "0" + "', '" + "5" + "')";
       return cn.Insert(sql2);
   }
}
        */
    }
}